# System Stat Monitor
A custom Aida64 Sensorpanel in a 3D printed case to show the most important system stats for the CPU, GPU and RAM:
<p align="center">
 <img src="img/1_systemstat.png?raw=true" alt="Skin"/>
</p>
<p align="center">
 <img src="img/1_front.jpg?raw=true" alt="Front"/>
</p>


# What you need?
 1. Waveshare 7 inch 1024*600 Capacitive Touch Screen LCD Display (https://www.amazon.co.uk/Waveshare-Capacitive-Interface-Raspberry-Beaglebone/dp/B015E8EDYQ)
 2. HDMI cable (included with display)
 3. USB micro cable
 4. Some screws (included with display)

# Software!
 1. You must install Aida64 (https://www.aida64.com/downloads) and RTSS Statistic (https://rivatuner-statistics-server.software.informer.com/) on your system. RTSS is only used for the FPS display on the bottom right. If you do not want the FPS metric, you do not need to install RTSS.
 2. Open the Sensorpanel in Aida64 and import the panel.
 3. That should be it.
 

# 3D-Printer!
Following case is used to mount the 7 inch display:
<p align="center">
 <img src="img/2_case.png?raw=true" alt="Case"/>
</p>

You need to print following two models:
  - https://www.thingiverse.com/thing:3743598 (piMac screen)
  - https://www.thingiverse.com/thing:3734539 (piMac foot)

*made with ♥ by jor
